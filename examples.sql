INSERT INTO students (first_name, second_name, gender) VALUES
  ('Ivan', 'German', 'MALE'),
  ('Alexander', 'Mikhalchuk', 'MALE'),
  ('Lora', 'Palmer', 'FEMALE');

SELECT * FROM students
WHERE gender = 'MALE';

INSERT INTO groups (name) VALUES
  ('KS-07a'),
  ('KS-07b');

SELECT * FROM groups;

INSERT INTO STUDENT_GROUP VALUES
  (1, 1),
  (2, 2),
  (3, 1),
  (3, 2);

SELECT * FROM STUDENT_GROUP;

-- | first_name | second_name | group_name |

SELECT first_name, second_name, name as group_name
FROM students AS s
JOIN student_group AS sg ON s.id = sg.student_id
JOIN groups g ON g.id = sg.group_id
WHERE g.name = 'KS-07a'
ORDER BY group_name, second_name;

DELETE FROM students
WHERE id > 3;

INSERT INTO students (first_name, second_name) VALUES
  ('John', 'Elthon');

-- | name | group_names |

SELECT CONCAT(first_name, ' ', second_name) AS name, GROUP_CONCAT(name SEPARATOR ', ') AS group_names
FROM students AS s
  JOIN student_group AS sg ON s.id = sg.student_id
  JOIN groups g ON g.id = sg.group_id
GROUP BY first_name, second_name
ORDER BY group_names, second_name;

UPDATE students
SET gender = 'MALE'
WHERE id = 7;
