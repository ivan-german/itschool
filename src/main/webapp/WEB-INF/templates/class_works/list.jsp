<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="title" value="IT School" />
<jsp:include page="../shared/_header.jsp" />

Current User: ${currentUser}

<%@include file="_classWorkList.jsp" %>

<%@include file="_newClassWork.jsp" %>

<jsp:include page="../shared/_footer.jsp" />
