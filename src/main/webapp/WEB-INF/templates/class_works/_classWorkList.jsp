<h3>Class works</h3>

<p>
<table>
    <tr>
        <th>Date</th>
        <th>Theme</th>
        <th>Home task</th>
    </tr>
    <c:forEach items="${classWorks}" var="cw">
        <tr>
            <td>${cw.dateString}</td>
            <td>${cw.theme}</td>
            <td>${cw.homeTask}</td>
        </tr>
    </c:forEach>
</table>
</p>
