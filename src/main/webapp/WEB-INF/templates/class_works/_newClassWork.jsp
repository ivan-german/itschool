<h3>Create new student</h3>

<c:if test="${not empty errors}">
    <p>
            ${errors}
    </p>
</c:if>

<form method="post">
    <input type="hidden" name="groupId" value="${group.id}" />
    <div>
        <label for="date">Date</label>
        <input type="date" name="date" id="date" value="${form.date}"/>
    </div>
    <div>
        <label for="theme">Theme</label>
        <input type="text" name="theme" id="theme" value="${form.theme}"/>
    </div>
    <div>
        <label for="homeTask">Home task</label>
        <input type="text" name="homeTask" id="homeTask" value="${form.homeTask}"/>
    </div>
    <div>
        <button type="submit">Submit</button>
    </div>
</form>
