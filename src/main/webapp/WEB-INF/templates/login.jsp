<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="title" value="IT School - Log in" />
<jsp:include page="shared/_header.jsp" />

<h3>Login</h3>

<c:if test="${error != null}">
    <div>
      ${error}
    </div>
</c:if>

<form action="/login" method="POST">
    <div>
        Login: <input type="text" name="login" />
    </div>
    <div>
        Password: <input type="password" name="password" />
    </div>
    <div>
    <button type="submit">Log in</button>
    </div>
</form>

<jsp:include page="shared/_footer.jsp" />