<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="title" value="IT School" />
<jsp:include page="shared/_header.jsp" />

<h3>IT School app</h3>
<p>
  <a href="/login">Log in</a>
</p>
<p>
  <a href="/registration">Sign up</a>
</p>

<jsp:include page="shared/_footer.jsp" />
