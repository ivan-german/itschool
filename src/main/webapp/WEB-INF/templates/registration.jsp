<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="title" value="IT School - Registration" />
<jsp:include page="shared/_header.jsp" />

<h3>Registration</h3>

<c:if test="${not empty errors}">
  <ul>
    <c:forEach items="${errors}" var="error">
      <li>${error}</li>
    </c:forEach>
  </ul>
</c:if>

<form action="/registration" method="POST">
  <div>
    Login:
    <input type="text" name="login" value="${form.login}" />
  </div>
  <div>
    Password:
    <input type="password" name="password" />
  </div>
  <div>
    Confirm password:
    <input type="password" name="password-confirmation" />
  </div>
  <div>
    Choose your name:
    <input type="text" name="name" value="${form.name}" />
  </div>
  <div>
    <button type="submit">Register</button>
  </div>
</form>

<jsp:include page="shared/_footer.jsp" />
