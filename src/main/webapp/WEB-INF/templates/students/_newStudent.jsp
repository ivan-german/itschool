<h3>Create new student</h3>

<c:if test="${not empty errors}">
    <p>
        ${errors}
    </p>
</c:if>

<form method="post">
    <input type="hidden" name="groupId" value="${group.id}" />
    <div>
        <label for="firstname">First name</label>
        <input type="text" name="firstname" id="firstname" value="${form.firstName}"/>
    </div>
    <div>
        <label for="secondname">Second name</label>
        <input type="text" name="secondname" id="secondname" value="${form.secondName}" />
    </div>
    <div>
        <label for="gender_male">Male</label>
        <input id="gender_male" type="radio" name="gender" value="MALE">

        <label for="gender_female">Female</label>
        <input id="gender_female" type="radio" name="gender" value="FEMALE">
    </div>
    <div>
        <button type="submit">Submit</button>
    </div>
</form>
