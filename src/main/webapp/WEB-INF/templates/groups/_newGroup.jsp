<h3>Create new student</h3>

<c:if test="${not empty errors}">
    <p>
            ${errors}
    </p>
</c:if>

<form method="post">
    <div>
        <label for="name">Group name</label>
        <input type="text" name="name" id="name" value="${form.name}"/>
    </div>
    <div>
        <button type="submit">Submit</button>
    </div>
</form>
