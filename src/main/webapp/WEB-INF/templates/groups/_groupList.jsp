<h3>List of groups</h3>

<p>
<ul>
    <c:forEach items="${groups}" var="g">
        <li><a href="/students?groupId=${g.id}">${g.name}</a></li>
    </c:forEach>
</ul>
</p>
