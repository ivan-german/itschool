<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>${title}</title>
</head>
<body>

<div>
  <c:if test="${isAuthorized}">
    <a href="/logout">Log out</a>
  </c:if>
</div>