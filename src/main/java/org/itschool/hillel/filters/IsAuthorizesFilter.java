package org.itschool.hillel.filters;

import org.itschool.hillel.util.AuthenticationUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class IsAuthorizesFilter extends HttpFilter {

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        request.setAttribute("isAuthorized", AuthenticationUtil.isAuthenticated(request));
        chain.doFilter(request, response);
    }
}
