package org.itschool.hillel.mappers;

import org.itschool.hillel.model.ClassWork;
import org.itschool.hillel.model.Group;
import org.itschool.hillel.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClassWorkMapper implements Mapper<ClassWork> {
    @Override
    public ClassWork map(ResultSet rs) throws SQLException {
        User teacher = new User(rs.getInt("teacher_id"), rs.getString("teacher_login"), rs.getString("teacher_password"), rs.getString("teacher_name"));
        Group group = new Group(rs.getInt("group_id"), rs.getString("group_name"), teacher);

        return new ClassWork(rs.getInt("id"), group, rs.getDate("census_date"), rs.getString("theme"), rs.getString("home_task"));
    }
}
