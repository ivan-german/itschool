package org.itschool.hillel.mappers;

import org.itschool.hillel.model.Group;
import org.itschool.hillel.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupMapper implements Mapper<Group> {

    @Override
    public Group map(ResultSet rs) throws SQLException {
        User teacher = new User(rs.getInt("teacher_id"),
                                rs.getString("teacher_login"),
                                rs.getString("teacher_password"),
                                rs.getString("teacher_name"));

        return new Group(rs.getInt("id"), rs.getString("name"), teacher);
    }
}
