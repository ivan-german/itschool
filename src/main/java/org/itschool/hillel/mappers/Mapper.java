package org.itschool.hillel.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Mapper<T> {
    T map(ResultSet rs) throws SQLException;
}
