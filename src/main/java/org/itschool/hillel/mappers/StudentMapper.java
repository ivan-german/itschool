package org.itschool.hillel.mappers;

import org.itschool.hillel.model.Gender;
import org.itschool.hillel.model.Student;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentMapper implements Mapper<Student> {

    @Override
    public Student map(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String firstName = rs.getString("first_name");
        String secondName = rs.getString("second_name");
        Gender gender = Gender.valueOf(rs.getString("gender"));

        return new Student(id, firstName, secondName, gender);
    }

}
