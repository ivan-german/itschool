package org.itschool.hillel.mappers;

import org.itschool.hillel.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements Mapper<User> {

    @Override
    public User map(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String login = rs.getString("login");
        String password = rs.getString("password");
        String name = rs.getString("name");

        return new User(id, login, password, name);
    }
}
