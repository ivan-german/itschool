package org.itschool.hillel.forms;

import org.itschool.hillel.validation.MultipleValues;
import org.itschool.hillel.validation.NotEmpty;

public class StudentForm {

    @NotEmpty(message = "First name should not be empty")
    public final String firstName;

    @NotEmpty(message = "Second name should not be empty")
    public final String secondName;

    @NotEmpty(message = "Gender should be set")
    @MultipleValues(values = {"MALE", "FEMALE"}, message = "Unknown gender")
    public final String gender;

    public StudentForm() {
        this.firstName = "";
        this.secondName = "";
        this.gender = "";
    }

    public StudentForm(String firstName, String secondName, String age, String gender) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getGender() {
        return gender;
    }
}
