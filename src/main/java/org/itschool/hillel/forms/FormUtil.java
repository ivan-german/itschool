package org.itschool.hillel.forms;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class FormUtil {

    public static <T> T readForm(HttpServletRequest req, Class<T> formClass) {
        T result = null;

        try {
            Field[] fields = formClass.getDeclaredFields();

            Constructor<T> defaultConstructor = formClass.getConstructor();
            result = defaultConstructor.newInstance();

            for (Field f : fields) {
                String fieldName = f.getName();
                String stringValue = req.getParameter(fieldName);

                Object value = null;
                Class expectedType = f.getType();

                if (String.class.equals(expectedType)) {
                    value = stringValue;
                } else if (Integer.class.equals(expectedType) || int.class.equals(expectedType)) {
                    value = Integer.parseInt(stringValue);
                }

                f.setAccessible(true);
                f.set(result, value);
                f.setAccessible(false);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return result;
    }

}
