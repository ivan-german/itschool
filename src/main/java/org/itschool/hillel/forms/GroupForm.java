package org.itschool.hillel.forms;

import org.itschool.hillel.validation.NotEmpty;

public class GroupForm {

    @NotEmpty(message = "Group name should be set")
    public final String name;

    public GroupForm() {
        this.name = "";
    }

    public GroupForm(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
