package org.itschool.hillel.forms;

import org.itschool.hillel.validation.NotEmpty;

public class ClassWorkForm {

    @NotEmpty(message = "Date of class is not set")
    public final String date;

    @NotEmpty(message = "Theme of class is not set")
    public final String theme;

    public final String homeTask;

    public ClassWorkForm() {
        this.date = "";
        this.theme = "";
        this.homeTask = "";
    }

    public ClassWorkForm(String date, String theme, String homeTask) {
        this.date = date;
        this.theme = theme;
        this.homeTask = homeTask;
    }

    public String getDate() {
        return date;
    }

    public String getTheme() {
        return theme;
    }

    public String getHomeTask() {
        return homeTask;
    }
}
