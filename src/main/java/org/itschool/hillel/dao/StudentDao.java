package org.itschool.hillel.dao;

import org.itschool.hillel.model.Group;
import org.itschool.hillel.model.Student;

import java.util.List;

public interface StudentDao {

    List<Student> list(Group group);
}
