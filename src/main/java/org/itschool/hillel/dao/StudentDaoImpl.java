package org.itschool.hillel.dao;

import org.itschool.hillel.mappers.StudentMapper;
import org.itschool.hillel.model.Group;
import org.itschool.hillel.model.Student;
import org.itschool.hillel.util.JdbcWrapper;

import java.util.List;

public class StudentDaoImpl implements StudentDao {

    private static StudentDaoImpl INSTANCE = new StudentDaoImpl();

    private StudentDaoImpl() {}

    public static StudentDaoImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public List<Student> list(Group group) {
        return JdbcWrapper.find(
                "SELECT s.* FROM students s " +
                "JOIN student_group sg ON s.id = sg.student_id " +
                "WHERE sg.group_id = ?",
                new StudentMapper(),
                group.getId());
    }
}
