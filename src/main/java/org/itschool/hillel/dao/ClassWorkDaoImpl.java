package org.itschool.hillel.dao;

import org.itschool.hillel.mappers.ClassWorkMapper;
import org.itschool.hillel.model.ClassWork;
import org.itschool.hillel.model.Group;
import org.itschool.hillel.util.JdbcWrapper;

import java.util.List;

public class ClassWorkDaoImpl implements ClassWorkDao {
    private static ClassWorkDaoImpl ourInstance = new ClassWorkDaoImpl();

    public static ClassWorkDaoImpl getInstance() {
        return ourInstance;
    }

    private ClassWorkDaoImpl() {
    }

    @Override
    public List<ClassWork> list(Group group) {
        return JdbcWrapper.find(
                "SELECT cw.*, " +
                        "g.id as group_id, " +
                        "g.name as group_name, " +
                        "u.id as teacher_id," +
                        "u.login as teacher_login, " +
                        "u.password as teacher_password, " +
                        "u.name as teacher_name " +
                "FROM class_works cw " +
                "JOIN groups g ON g.id = cw.group_id " +
                "JOIN users u ON g.teacher_id = u.id " +
                "WHERE g.id = ? " +
                "ORDER BY cw.census_date",
                new ClassWorkMapper(),
                group.getId());
    }

    @Override
    public void save(ClassWork classWork) {
        JdbcWrapper.update("INSERT INTO class_works (census_date, theme, home_task, group_id) VALUES (?, ?, ?, ?)",
                classWork.getDate(),
                classWork.getTheme(),
                classWork.getHomeTask(),
                classWork.getGroup().getId());
    }
}
