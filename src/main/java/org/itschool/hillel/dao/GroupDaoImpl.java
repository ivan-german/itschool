package org.itschool.hillel.dao;

import org.itschool.hillel.mappers.GroupMapper;
import org.itschool.hillel.model.Group;
import org.itschool.hillel.model.Student;
import org.itschool.hillel.model.User;
import org.itschool.hillel.util.JdbcWrapper;

import java.util.List;

public class GroupDaoImpl implements GroupDao {

    private final static GroupDaoImpl INSTANCE = new GroupDaoImpl();

    private GroupDaoImpl() {}

    public static GroupDaoImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public List<Group> getGroups(User teacher) {
        return JdbcWrapper.find(
                "SELECT g.id AS id, " +
                        "g.name AS name, " +
                        "u.id AS teacher_id, " +
                        "u.login AS teacher_login, " +
                        "u.password AS teacher_password, " +
                        "u.name AS teacher_name " +
                "FROM groups g " +
                "JOIN users u " +
                "ON g.teacher_id = u.id " +
                "WHERE teacher_id = ?",
                new GroupMapper(),
                teacher.getId());
    }

    @Override
    public Group get(int groupId) {
        List<Group> result = JdbcWrapper.find(
                "SELECT g.id AS id, " +
                        "g.name AS name, " +
                        "u.id AS teacher_id, " +
                        "u.login AS teacher_login, " +
                        "u.password AS teacher_password, " +
                        "u.name AS teacher_name " +
                "FROM groups g " +
                "JOIN users u " +
                "ON g.teacher_id = u.id " +
                "WHERE g.id = ?",
                new GroupMapper(),
                groupId);

        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public void addStudent(Group group, Student student) {
        List<Integer> ids = JdbcWrapper.insert(
                "INSERT INTO students (first_name, second_name, gender) VALUES (?, ?, ?)",
                student.getFirstName(),
                student.getSecondName(),
                student.getGender().toString());

        JdbcWrapper.update("INSERT INTO student_group (student_id, group_id) VALUES (?, ?)",
                ids.get(0), group.getId());
    }

    @Override
    public void create(Group group) {
        User teacher = group.getTeacher();

        if (teacher == null) throw new IllegalArgumentException("Teacher not set in group to save");

        JdbcWrapper.update(
                "INSERT INTO groups (name, teacher_id) VALUES (?, ?)",
                group.getName(),
                group.getTeacher().getId());
    }
}
