package org.itschool.hillel.dao;

import org.itschool.hillel.model.User;

public interface UserDao {

    void save(User user);

    User getUser(String username);

    boolean isUserExist(User user);

}
