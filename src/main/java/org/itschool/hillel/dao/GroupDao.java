package org.itschool.hillel.dao;

import org.itschool.hillel.model.Group;
import org.itschool.hillel.model.Student;
import org.itschool.hillel.model.User;

import java.util.List;

public interface GroupDao {

    void create(Group group);

    Group get(int groupId);
    List<Group> getGroups(User teacher);

    void addStudent(Group group, Student student);

}
