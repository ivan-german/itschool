package org.itschool.hillel.dao;

import org.itschool.hillel.mappers.UserMapper;
import org.itschool.hillel.model.User;
import org.itschool.hillel.util.JdbcWrapper;

import java.util.List;

public class UserDaoImpl implements UserDao {

    private final static UserDaoImpl INSTANCE = new UserDaoImpl();

    private UserDaoImpl() {}

    public static UserDaoImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public void save(User user) {
        JdbcWrapper.update(
                "INSERT INTO users (login, password, name) VALUES (?, ?, ?)",
                user.getLogin(),
                user.getPassword(),
                user.getName());
    }

    @Override
    public User getUser(String username) {
        List<User> foundUsers = JdbcWrapper
                .find("SELECT * FROM users WHERE login = ?", new UserMapper(), username);

        return foundUsers.isEmpty() ? null : foundUsers.get(0);
    }

    @Override
    public boolean isUserExist(User user) {
        return !JdbcWrapper.find("SELECT * FROM users WHERE login = ? AND password = ?",
                new UserMapper(),
                user.getLogin(),
                user.getPassword()).isEmpty();
    }
}
