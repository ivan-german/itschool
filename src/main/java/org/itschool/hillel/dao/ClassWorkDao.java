package org.itschool.hillel.dao;

import org.itschool.hillel.model.ClassWork;
import org.itschool.hillel.model.Group;

import java.util.List;

public interface ClassWorkDao {
    List<ClassWork> list(Group group);
    void save(ClassWork classWork);
}
