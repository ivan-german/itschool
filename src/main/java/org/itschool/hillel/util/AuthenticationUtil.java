package org.itschool.hillel.util;

import javax.servlet.http.HttpServletRequest;

public class AuthenticationUtil {

    public static void authencate(HttpServletRequest req, String username) {
        req.getSession().setAttribute("currentUser", username);
    }

    public static String currentUser(HttpServletRequest req) {
        return req.getSession().getAttribute("currentUser").toString();
    }

    public static boolean isAuthenticated(HttpServletRequest req) {
        return req.getSession().getAttribute("currentUser") != null;
    }

    public static void logout(HttpServletRequest req) {
        req.getSession().invalidate();
    }

}
