package org.itschool.hillel.util;

import org.itschool.hillel.mappers.Mapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JdbcWrapper {

    static {
        try {
            Class.forName(PropertiesUtil.getProperty("db.driver"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static <T> List<T> find(String sql, Mapper<T> mapper, Object... params) {
        List<T> result = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(sql)) {

            for (int i = 0; i < params.length; i++) {
                stmt.setObject(i + 1, params[i]);
            }

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.add(mapper.map(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void update(String sql, Object... params) {

        try (Connection con = getConnection();
             PreparedStatement stmt = con.prepareStatement(sql)) {

            for (int i = 0; i < params.length; i++) {
                Object param = params[i];
                stmt.setObject(i + 1, param);
            }

            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /*
        В отличие от метода update возвращает список ID для объектов,
        которые были вставлены.

        Метод крайне полезен для сохранения записей, затрагивающих
        более одной таблицы.
    */
    public static List<Integer> insert(String sql, Object... params) {
        List<Integer> generatedIds = new LinkedList<>();

        try (Connection con = getConnection();
             PreparedStatement stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            for (int i = 0; i < params.length; i++) {
                Object param = params[i];
                stmt.setObject(i + 1, param);
            }

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            while (rs.next()) {
                generatedIds.add(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return generatedIds;
    }

    private static Connection getConnection() throws SQLException {
        String url = PropertiesUtil.getProperty("db.url");
        String username = PropertiesUtil.getProperty("db.username");
        String password = PropertiesUtil.getProperty("db.password");

        return DriverManager.getConnection(url, username, password);
    }

}
