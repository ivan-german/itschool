package org.itschool.hillel.controller;

import org.itschool.hillel.forms.LoginForm;
import org.itschool.hillel.model.User;
import org.itschool.hillel.dao.UserDao;
import org.itschool.hillel.dao.UserDaoImpl;
import org.itschool.hillel.util.AuthenticationUtil;
import org.itschool.hillel.forms.FormUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/templates/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LoginForm form = FormUtil.readForm(req, LoginForm.class);
        User user = formToUser(form);

        UserDao userDao = UserDaoImpl.getInstance();

        if (!userDao.isUserExist(user)) {
            req.setAttribute("error", "User not exist");
            req.getRequestDispatcher("WEB-INF/templates/login.jsp").forward(req, resp);
        }

        AuthenticationUtil.authencate(req, form.getLogin());
        resp.sendRedirect("/groups");
    }

    private User formToUser(LoginForm form) {
        return new User(null, form.getLogin(), form.getPassword(), null);
    }
}
