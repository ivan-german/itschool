package org.itschool.hillel.controller;

import org.itschool.hillel.forms.RegistrationForm;
import org.itschool.hillel.model.User;
import org.itschool.hillel.dao.UserDao;
import org.itschool.hillel.dao.UserDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/registration.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RegistrationForm form = readForm(req);

        List<String> errors = validate(form);

        if (errors.size() > 0) {
            req.setAttribute("errors", errors);
            req.setAttribute("form", form);
            req.getRequestDispatcher("/WEB-INF/templates/registration.jsp")
                    .forward(req, resp);
        }

        User user = formToUser(form);

        UserDao userDao = UserDaoImpl.getInstance();
        userDao.save(user);

        resp.sendRedirect("/index.html");
    }

    private RegistrationForm readForm(HttpServletRequest req) {
        RegistrationForm form = new RegistrationForm();

        form.setLogin(req.getParameter("login"));
        form.setPassword(req.getParameter("password"));
        form.setPasswordConfirmation(req.getParameter("password-confirmation"));
        form.setName(req.getParameter("name"));

        return form;
    }

    private List<String> validate(RegistrationForm form) {
        List<String> errors = new LinkedList<String>();

        if (form.getLogin().trim().length() == 0) {
            errors.add("Login should not be empty");
        }

        if (form.getPassword().trim().length() == 0) {
            errors.add("Password should not be empty");
        }

        if (form.getName().trim().length() == 0) {
            errors.add("Name should not be empty");
        }

        if (!form.getPassword().equals(form.getPasswordConfirmation())) {
            errors.add("Password confirmation should match password");
        }

        return errors;
    }

    private User formToUser(RegistrationForm form) {
        return new User(
                null,
                form.getLogin(),
                form.getPassword(),
                form.getName());
    }
}
