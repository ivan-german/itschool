package org.itschool.hillel.controller;

import org.itschool.hillel.dao.*;
import org.itschool.hillel.forms.FormUtil;
import org.itschool.hillel.forms.GroupForm;
import org.itschool.hillel.model.Group;
import org.itschool.hillel.model.User;
import org.itschool.hillel.validation.Validator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.itschool.hillel.util.AuthenticationUtil.currentUser;

@WebServlet("/groups")
public class GroupsController extends HttpServlet {

    public GroupDao groupDao = GroupDaoImpl.getInstance();
    public UserDao userDao = UserDaoImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        showGroupListPage(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GroupForm form = FormUtil.readForm(req, GroupForm.class);
        List<String> errors = Validator.validate(form);

        if (errors.size() > 0) {
            req.setAttribute("errors", errors);
            req.setAttribute("form", form);
            showGroupListPage(req, resp);
        } else {
            Group group = formToModel(form, currentUser(req));
            groupDao.create(group);
            resp.sendRedirect("/groups");
        }
    }

    private void showGroupListPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = currentUser(req);
        req.setAttribute("groups", groupDao.getGroups(userDao.getUser(userName)));
        req.getRequestDispatcher("WEB-INF/templates/groups/list.jsp").forward(req, resp);
    }

    private Group formToModel(GroupForm form, String teacherName) {
        User teacher = userDao.getUser(teacherName);
        return new Group(form.getName(), teacher);
    }
}
