package org.itschool.hillel.controller;

import org.itschool.hillel.dao.GroupDao;
import org.itschool.hillel.dao.GroupDaoImpl;
import org.itschool.hillel.dao.StudentDao;
import org.itschool.hillel.dao.StudentDaoImpl;
import org.itschool.hillel.forms.StudentForm;
import org.itschool.hillel.model.Gender;
import org.itschool.hillel.model.Group;
import org.itschool.hillel.model.Student;
import org.itschool.hillel.validation.Validator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/students")
public class StudentsServlet extends HttpServlet {

    public StudentDao studentDao = StudentDaoImpl.getInstance();
    public GroupDao groupDao = GroupDaoImpl.getInstance();

    /**
     * Получение списка студентов для группы
     */
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        showStudentListPage(req, resp, new StudentForm());
    }

    @Override
    /**
     * Создание нового студента в группе
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        StudentForm form = readStudent(req);
        List<String> errors = Validator.validate(form);

        if (errors.size() > 0) {
            req.setAttribute("errors", errors);
            showStudentListPage(req, resp, form);
        } else {
            Student student = formToStudent(form);
            Group group = getGroup(req);

            groupDao.addStudent(group, student);

            resp.sendRedirect("/students?groupId=" + group.getId());
        }
    }

    private void showStudentListPage(HttpServletRequest req, HttpServletResponse resp, StudentForm form) throws ServletException, IOException {
        Group group = getGroup(req);
        List<Student> students = studentDao.list(group);

        req.setAttribute("group", group);
        req.setAttribute("students", students);
        req.setAttribute("form", form);

        req.getRequestDispatcher("/WEB-INF/templates/students/list.jsp").forward(req, resp);
    }

    private StudentForm readStudent(HttpServletRequest req) {
        return new StudentForm(req.getParameter("firstname"),
                req.getParameter("secondname"),
                req.getParameter("age"),
                req.getParameter("gender"));
    }

    private Group getGroup(HttpServletRequest req) {
        int groupId = Integer.parseInt(req.getParameter("groupId"));
        return groupDao.get(groupId);
    }

    private Student formToStudent(StudentForm form) {
            Gender gender = form.getGender() == null
                    ? Gender.UNKNOWN
                    : Gender.valueOf(form.getGender().toUpperCase());

            return new Student(null, form.getFirstName(), form.getSecondName(), gender);
    }

}
