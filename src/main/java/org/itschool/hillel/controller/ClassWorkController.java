package org.itschool.hillel.controller;

import org.itschool.hillel.dao.ClassWorkDao;
import org.itschool.hillel.dao.ClassWorkDaoImpl;
import org.itschool.hillel.dao.GroupDao;
import org.itschool.hillel.dao.GroupDaoImpl;
import org.itschool.hillel.forms.ClassWorkForm;
import org.itschool.hillel.forms.FormUtil;
import org.itschool.hillel.model.ClassWork;
import org.itschool.hillel.model.Group;
import org.itschool.hillel.util.DateUtil;
import org.itschool.hillel.validation.Validator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

@WebServlet("/class-works")
public class ClassWorkController extends HttpServlet {

    public GroupDao groupDao = GroupDaoImpl.getInstance();
    public ClassWorkDao classWorkDao = ClassWorkDaoImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        showClassWorkListPage(req, resp, new ClassWorkForm());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ClassWorkForm form = FormUtil.readForm(req, ClassWorkForm.class);
        List<String> errors = Validator.validate(form);

        if (errors.size() > 0) {
            req.setAttribute("errors", errors);
            showClassWorkListPage(req, resp, form);
        } else {
            Group group = getGroup(req);
            ClassWork classWork = formToModel(form, group);
            classWorkDao.save(classWork);
            resp.sendRedirect("/class-works?groupId=" + group.getId());
        }
    }

    private void showClassWorkListPage(HttpServletRequest req, HttpServletResponse resp, ClassWorkForm form) throws ServletException, IOException {
        Group group = getGroup(req);
        List<ClassWork> classWorks = classWorkDao.list(group);

        req.setAttribute("group", group);
        req.setAttribute("classWorks", classWorks);
        req.setAttribute("form", form);

        req.getRequestDispatcher("WEB-INF/templates/class_works/list.jsp").forward(req, resp);
    }

    private Group getGroup(HttpServletRequest req) {
        int groupId = Integer.parseInt(req.getParameter("groupId"));
        return groupDao.get(groupId);
    }

    private ClassWork formToModel(ClassWorkForm form, Group group) {

        try {
            return new ClassWork(null,
                                 group,
                                 DateUtil.format.parse(form.getDate()),
                                 form.getTheme(),
                                 form.getHomeTask());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
