package org.itschool.hillel.model;

public class Group {
    private Integer id;
    private String name;
    private User teacher;

    public Group() {
    }

    public Group(String name, User teacher) {
        this.name = name;
        this.teacher = teacher;
    }

    public Group(Integer id, String name, User teacher) {
        this.id = id;
        this.name = name;
        this.teacher = teacher;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getTeacher() {
        return teacher;
    }

    public void setTeacher(User teacher) {
        this.teacher = teacher;
    }
}
