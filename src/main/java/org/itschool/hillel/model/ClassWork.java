package org.itschool.hillel.model;

import org.itschool.hillel.util.DateUtil;

import java.util.Date;

public class ClassWork {
    private Integer id;
    private Group group;
    private java.sql.Date date;
    private String theme;
    private String homeTask;

    public ClassWork() {
    }

    public ClassWork(Integer id, Group group, Date date, String theme, String homeTask) {
        this.id = id;
        this.group = group;
        this.date = new java.sql.Date(date.getTime());
        this.theme = theme;
        this.homeTask = homeTask;
    }

    public ClassWork(Integer id, Group group, java.sql.Date date, String theme, String homeTask) {
        this.id = id;
        this.group = group;
        this.date = date;
        this.theme = theme;
        this.homeTask = homeTask;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getDateString() {
        return DateUtil.format.format(date);
    }

    public java.sql.Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = new java.sql.Date(date.getTime());
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getHomeTask() {
        return homeTask;
    }

    public void setHomeTask(String homeTask) {
        this.homeTask = homeTask;
    }
}
