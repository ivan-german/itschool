package org.itschool.hillel.model;

public enum Gender {
    MALE {
        public String getAddressment() {
            return "Mr.";
        }
    },
    FEMALE {
        public String getAddressment() {
            return "Mrs.";
        }
    },
    UNKNOWN {
        public String getAddressment() {
            return "";
        }
    };

    public abstract String getAddressment();
}
