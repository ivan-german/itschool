package org.itschool.hillel.validation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Validator {

    public static List<String> validate(Object form) {
        List<String> result = new ArrayList<>();

        try {
            Class formClass = form.getClass();
            Field[] fields = formClass.getFields();

            for (Field field : fields) {
                boolean accessible = field.isAccessible();
                field.setAccessible(true);

                notEmptyCheck(form, result, field);
                multipleValuesCheck(form, result, field);

                field.setAccessible(accessible);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException("Validation failed");
        }

        return result;
    }

    private static void multipleValuesCheck(Object form, List<String> result, Field field) throws IllegalAccessException {
        MultipleValues multipleValues = field.getAnnotation(MultipleValues.class);

        if (multipleValues != null) {
            Object value = field.get(form);

            if (value != null && !(value instanceof String)) {
                throw new RuntimeException("@MultipleValues can be applied only on Strings");
            }

            String[] values = multipleValues.values();
            if (!Arrays.asList(values).contains(value)) {
                String message = multipleValues.message();
                result.add(message);
            }
        }
    }

    private static void notEmptyCheck(Object form, List<String> result, Field field) throws IllegalAccessException {
        NotEmpty notEmpty = field.getAnnotation(NotEmpty.class);
        if (notEmpty != null) {
            Object value = field.get(form);

            if (value != null && !(value instanceof String)) {
                throw new RuntimeException("@NotEmpty can be applied only on Strings");
            }

            String stringValue = (String) value;

            if (stringValue == null || stringValue.trim().isEmpty()) {
                result.add(notEmpty.message());
            }
        }
    }

}
