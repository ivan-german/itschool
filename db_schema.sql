CREATE TABLE users (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  login VARCHAR(256) NOT NULL,
  password VARCHAR(256) NOT NULL,
  name VARCHAR(256)
);

CREATE TABLE students (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(255) NOT NULL,
  second_name VARCHAR(255) NOT NULL,
  gender VARCHAR(16) NOT NULL DEFAULT 'UNKNOWN'
);

CREATE TABLE groups (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255),
  teacher_id INTEGER REFERENCES users(id)
);

CREATE TABLE student_group (
  student_id INTEGER REFERENCES students (id) REFERENCES students(id),
  group_id INTEGER REFERENCES groups (id) REFERENCES groups(id),
  PRIMARY KEY (student_id, group_id)
);

CREATE TABLE class_works (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  census_date DATE NOT NULL,
  theme VARCHAR(255) NOT NULL,
  home_task VARCHAR(255),
  group_id INTEGER REFERENCES groups (id) REFERENCES groups(id)
);
